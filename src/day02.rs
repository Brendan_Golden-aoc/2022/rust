enum RPS {
    Rock,
    Paper,
    Scissors,
    Null,
}

pub fn generator(input: &str) -> Vec<[char; 2]> {
    let mut result = Vec::new();

    for line in input.split('\n') {
        let mut tmp = line.chars();
        let first = tmp.next().unwrap_or_default();
        tmp.next();
        let second = tmp.next().unwrap_or_default();
        let temp = [first, second];
        result.push(temp);
    }

    result
}

pub fn part1_1(input: &[[char; 2]]) -> i32 {
    let mut score = 0;

    for round in input {
        let foe = char_to_enum(round[0]);
        let you = char_to_enum(round[1]);

        score += rps(foe, you) as i32;
    }

    score
}

fn char_to_enum(t: char) -> RPS {
    match t {
        'A' => RPS::Rock,
        'B' => RPS::Paper,
        'C' => RPS::Scissors,
        'X' => RPS::Rock,
        'Y' => RPS::Paper,
        'Z' => RPS::Scissors,
        _ => RPS::Null,
    }
}

fn rps(foe: RPS, you: RPS) -> u8 {
    match you {
        RPS::Rock => match foe {
            RPS::Rock => 1 + 3,
            RPS::Paper => 1,
            RPS::Scissors => 1 + 6,
            _ => 0,
        },
        RPS::Paper => match foe {
            RPS::Rock => 2 + 6,
            RPS::Paper => 2 + 3,
            RPS::Scissors => 2,
            _ => 0,
        },
        RPS::Scissors => match foe {
            RPS::Rock => 3,
            RPS::Paper => 3 + 6,
            RPS::Scissors => 3 + 3,
            _ => 0,
        },
        _ => 0,
    }
}

pub fn part2_1(input: &[[char; 2]]) -> i32 {
    let mut score = 0;

    for round in input {
        let foe = char_to_enum(round[0]);
        let you = get_result(&foe, round[1]);

        score += rps(foe, you) as i32;
    }

    score
}

fn get_result(foe: &RPS, outcome: char) -> RPS {
    match foe {
        RPS::Rock => {
            match outcome {
                // loose
                'X' => RPS::Scissors,
                // draw
                'Y' => RPS::Rock,
                // win
                'Z' => RPS::Paper,
                _ => RPS::Null,
            }
        }
        RPS::Paper => {
            match outcome {
                // loose
                'X' => RPS::Rock,
                // draw
                'Y' => RPS::Paper,
                // win
                'Z' => RPS::Scissors,
                _ => RPS::Null,
            }
        }
        RPS::Scissors => {
            match outcome {
                // loose
                'X' => RPS::Paper,
                // draw
                'Y' => RPS::Scissors,
                // win
                'Z' => RPS::Rock,
                _ => RPS::Null,
            }
        }
        _ => RPS::Null,
    }
}

pub fn part1_2(input: &[[char; 2]]) -> i32 {
    let mut score = 0;

    for round in input {
        let foe = RPS2::char_to_enum_part1(round[0]);
        let you = RPS2::char_to_enum_part1(round[1]);
        let outcome = you.outcome(foe);
        score += you.score(outcome);
    }

    score
}

pub fn part2_2(input: &[[char; 2]]) -> i32 {
    let mut score = 0;

    for round in input {
        let foe = RPS2::char_to_enum_part1(round[0]);
        let outcome = RPS2::char_to_enum_part2(round[1]);
        let you = foe.get_outcome(outcome);
        score += you.score(outcome);
    }

    score
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
enum RPS2 {
    Rock,
    Paper,
    Scissors,
    Null,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
enum Outcome {
    Win,
    Lose,
    Draw,
}

impl RPS2 {
    fn beats(self) -> Self {
        match self {
            RPS2::Rock => RPS2::Scissors,
            RPS2::Paper => RPS2::Rock,
            RPS2::Scissors => RPS2::Paper,
            RPS2::Null => RPS2::Null,
        }
    }

    pub fn outcome(self, other: Self) -> Outcome {
        if self.beats() == other {
            Outcome::Win
        } else if self == other {
            Outcome::Draw
        } else {
            Outcome::Lose
        }
    }

    pub fn get_outcome(self, required: Outcome) -> Self {
        if required == Outcome::Draw {
            self
        } else if required == Outcome::Lose {
            self.beats()
        } else {
            self.beats().beats()
        }
    }

    pub fn score(self, outcome: Outcome) -> i32 {
        let score_type = match self {
            RPS2::Rock => 1,
            RPS2::Paper => 2,
            RPS2::Scissors => 3,
            _ => 0,
        };

        let score_outcome = match outcome {
            Outcome::Win => 6,
            Outcome::Draw => 3,
            Outcome::Lose => 0,
        };

        score_type + score_outcome
    }

    pub fn char_to_enum_part1(t: char) -> Self {
        match t {
            'A' => RPS2::Rock,
            'B' => RPS2::Paper,
            'C' => RPS2::Scissors,
            'X' => RPS2::Rock,
            'Y' => RPS2::Paper,
            'Z' => RPS2::Scissors,
            _ => RPS2::Null,
        }
    }

    pub fn char_to_enum_part2(t: char) -> Outcome {
        match t {
            'X' => Outcome::Lose,
            'Y' => Outcome::Draw,
            'Z' => Outcome::Win,
            _ => Outcome::Lose,
        }
    }
}

// cursed, using https://github.com/ephemient/aoc2022/blob/main/kt/src/commonMain/kotlin/com/github/ephemient/aoc2022/Day2.kt as a "guide"
pub fn part1_3(input: &[[char; 2]]) -> i32 {
    let mut score = 0;

    for round in input {
        let foe = (round[0] as i32 - 'A' as i32) + 1;
        let you = (round[1] as i32 - 'X' as i32) + 1;
        let mut tmp = (((1 + you - foe) % 3) * 3) + you;
        if tmp == -2 {
            tmp = 7;
        }

        score += tmp;
    }

    score
}

pub fn part2_3(input: &[[char; 2]]) -> i32 {
    let mut score = 0;

    for round in input {
        let foe = (round[0] as i32 - 'A' as i32) + 1;
        let second = (round[1] as i32 - 'X' as i32) + 1;
        let you = 1 + ((foe + second) % 3);
        let mut tmp = (((1 + you - foe) % 3) * 3) + you;

        if tmp == -2 {
            tmp = 7;
        }

        score += tmp;
    }

    score
}

pub fn generator_4(input: &str) -> Vec<String> {
    let mut result = Vec::new();

    for line in input.split('\n') {
        result.push(line.to_owned());
    }

    result
}

pub fn part1_4(input: &[String]) -> i32 {
    let mut score = 0;

    for round in input {
        match round as &str {
            "A X" => score += 4,
            "A Y" => score += 8,
            "A Z" => score += 3,
            "B X" => score += 1,
            "B Y" => score += 5,
            "B Z" => score += 9,
            "C X" => score += 7,
            "C Y" => score += 2,
            "C Z" => score += 6,
            _ => score += 0,
        }
    }

    score
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &'static str = "A Y
B X
C Z";

    #[test]
    fn part1_test() {
        assert_eq!(part1_1(&generator(EXAMPLE)), 15);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2_1(&generator(EXAMPLE)), 12);
    }

    #[test]
    fn part1_2_test() {
        assert_eq!(part1_2(&generator(EXAMPLE)), 15);
    }

    #[test]
    fn part2_2_test() {
        assert_eq!(part2_2(&generator(EXAMPLE)), 12);
    }

    #[test]
    fn part1_3_test() {
        assert_eq!(part1_3(&generator(EXAMPLE)), 15);
    }

    #[test]
    fn part2_3_test() {
        assert_eq!(part2_3(&generator(EXAMPLE)), 12);
    }

    #[test]
    fn part1_4_test() {
        assert_eq!(part1_4(&generator_4(EXAMPLE)), 15);
    }
}
