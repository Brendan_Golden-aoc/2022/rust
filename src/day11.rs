#[derive(Debug, Clone)]
pub struct Monkey {
    items: Vec<i64>,
    operation: Op,
    divisible: Divisible,
    inspected: i64,
}
#[derive(Debug, Clone)]
pub enum Op {
    Plus(i64),
    Mult(i64),
}
#[derive(Debug, Clone)]
pub struct Divisible {
    divider: i64,
    m_true: usize,
    m_false: usize,
}

pub fn generator(input: &str) -> Vec<Monkey> {
    let mut instructions = vec![];

    for monkey in input.split("\n\n") {
        let mut items = vec![];
        let mut divisible = Divisible { divider: 0, m_true: 0, m_false: 0 };
        let mut operation = Op::Mult(0);
        for line in monkey.split('\n') {
            if line.starts_with("  Starting items: ") {
                items = line.replace("  Starting items: ", "").split(", ").map(|s| s.parse().unwrap_or(0)).collect::<Vec<_>>();
            } else if line.starts_with("  Operation: new = old ") {
                let tmp = line.replace("  Operation: new = old ", "");
                let remainder = tmp.split(' ').collect::<Vec<_>>();

                if remainder.len() < 2 {
                    continue;
                }

                let value = if remainder[1] == "old" {
                    -1
                } else if let Ok(x) = remainder[1].parse() {
                    x
                } else {
                    0
                };
                match remainder[0] {
                    "*" => {
                        operation = Op::Mult(value);
                    }
                    "+" => {
                        operation = Op::Plus(value);
                    }
                    &_ => {}
                }
            } else if line.starts_with("  Test: divisible by ") {
                if let Ok(x) = line.replace("  Test: divisible by ", "").parse() {
                    divisible.divider = x;
                }
            } else if line.starts_with("    If true: throw to monkey ") {
                if let Ok(x) = line.replace("    If true: throw to monkey ", "").parse() {
                    divisible.m_true = x;
                }
            } else if line.starts_with("    If false: throw to monkey ") {
                if let Ok(x) = line.replace("    If false: throw to monkey ", "").parse() {
                    divisible.m_false = x;
                }
            }
        }

        instructions.push(Monkey { items, operation, divisible, inspected: 0 });
    }

    instructions
}

pub fn part1_1(input: &[Monkey]) -> i64 {
    solver(input, 20, true)
}

fn solver(input: &[Monkey], rounds: i32, smol: bool) -> i64 {
    let mut monkeys = input.to_owned();

    let divider = if smol { 3 } else { monkeys.iter().map(|s| s.divisible.divider).product() };

    let mut counter = 0;
    loop {
        let mut monkey_index = 0;

        loop {
            let mut monkey = monkeys[monkey_index].clone();

            for item in &monkey.items {
                let mut new_item = *item;

                // add worry
                match monkey.operation {
                    Op::Plus(x) => {
                        if x < 0 {
                            new_item *= 2;
                        } else {
                            new_item += x;
                        }
                    }
                    Op::Mult(x) => {
                        if x < 0 {
                            new_item *= new_item;
                        } else {
                            new_item *= x;
                        }
                    }
                }

                // divide by e
                if smol {
                    new_item /= divider;
                } else {
                    // only if its above this value does it really do anything
                    new_item %= divider;
                };

                // throw to the next monkey
                if new_item % monkey.divisible.divider == 0 {
                    monkeys[monkey.divisible.m_true].items.push(new_item);
                } else {
                    monkeys[monkey.divisible.m_false].items.push(new_item);
                }
            }

            // all items dealt with, clear it out
            monkey.inspected += monkey.items.len() as i64;
            monkey.items = vec![];
            monkeys[monkey_index] = monkey;

            monkey_index += 1;
            if monkey_index >= monkeys.len() {
                break;
            }
        }

        counter += 1;
        if counter >= rounds {
            break;
        }
    }

    let mut max = vec![0, 0];

    for monkey in &monkeys {
        // println!("{:?}", monkey);
        // get max two
        max.push(monkey.inspected);
    }
    max.sort();
    max.reverse();

    max[0] * max[1]
}

pub fn part2_1(input: &[Monkey]) -> i64 {
    solver(input, 10000, false)
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &'static str = "\
Monkey 0:
  Starting items: 79, 98
  Operation: new = old * 19
  Test: divisible by 23
    If true: throw to monkey 2
    If false: throw to monkey 3

Monkey 1:
  Starting items: 54, 65, 75, 74
  Operation: new = old + 6
  Test: divisible by 19
    If true: throw to monkey 2
    If false: throw to monkey 0

Monkey 2:
  Starting items: 79, 60, 97
  Operation: new = old * old
  Test: divisible by 13
    If true: throw to monkey 1
    If false: throw to monkey 3

Monkey 3:
  Starting items: 74
  Operation: new = old + 3
  Test: divisible by 17
    If true: throw to monkey 0
    If false: throw to monkey 1";

    const TESTS: [(&str, i64, i64); 1] = [
        // formatting
        (EXAMPLE, 10605, 2713310158),
    ];

    #[test]
    fn part1_test() {
        for test in TESTS {
            assert_eq!(part1_1(&generator(test.0)), test.1);
        }
    }

    #[test]
    fn part2_test() {
        for test in TESTS {
            assert_eq!(part2_1(&generator(test.0)), test.2);
        }
    }
}
