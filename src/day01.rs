pub fn generator(input: &str) -> Vec<Vec<i32>> {
    let mut result: Vec<Vec<i32>> = Vec::new();

    let mut elf = vec![];
    for s in input.split('\n') {
        if s.is_empty() {
            result.push(elf);
            elf = vec![];
        } else {
            elf.push(s.parse::<i32>().unwrap())
        }
    }

    result.push(elf);

    result
}

pub fn part1_1(input: &[Vec<i32>]) -> i32 {
    let mut max = 0;

    for elf in input {
        let mut calories = 0;

        for item in elf {
            calories += item;
        }

        if calories > max {
            max = calories;
        }
    }

    max
}

pub fn part2_1(input: &[Vec<i32>]) -> i32 {
    let mut results = [0, 0, 0];

    for elf in input {
        let mut calories = 0;

        for item in elf {
            calories += item;
        }

        if calories > results[0] {
            results[0] = calories;
            results.sort();
        }
    }

    results.iter().sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &'static str = "1000
2000
3000

4000

5000
6000

7000
8000
9000

10000";

    #[test]
    fn part1_test() {
        assert_eq!(part1_1(&generator(EXAMPLE)), 24000);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2_1(&generator(EXAMPLE)), 45000);
    }
}
