use std::collections::VecDeque;

#[derive(Debug, Clone)]
pub enum Instruction {
    NoOp,
    AddX(i64),
}

pub fn generator(input: &str) -> Vec<Instruction> {
    let mut instructions = vec![];

    for line in input.split('\n') {
        let split = line.split(' ').collect::<Vec<_>>();
        match split[0] {
            "noop" => {
                instructions.push(Instruction::NoOp);
            }
            "addx" => {
                if split.len() < 2 {
                    continue;
                }

                let value = split[1].parse().unwrap_or(0);
                instructions.push(Instruction::AddX(value));
            }
            &_ => {}
        }
    }

    instructions
}

fn run_cycles(input: &[Instruction], cycles: usize) -> (i64, i64, String) {
    // new thing for me
    let mut upcoming_x_changes = VecDeque::new();
    let mut x = 1;
    let mut signal = 0;
    let mut output = vec![];

    for cycle in 1..=cycles {
        // handle drawing ehre

        let pixel = (cycle - 1) % 40;
        //  println!("{:?}",output);
        //println!("Cycle: {cycle} pixel: {pixel} x: {x}");

        if pixel == 0 && !output.is_empty() {
            output.push('\n');
        }

        if pixel == (x - 1) as usize || pixel == x as usize || pixel == (x + 1) as usize {
            output.push('█');
        } else {
            output.push('░');
        }

        //println!("Start cycles: {cycles} cycle: {cycle} x: {x} {:?}", upcoming_x_changes);

        // get teh ent instruction
        if let Some(instruction) = input.get(cycle - 1) {
            match instruction {
                Instruction::NoOp => {
                    upcoming_x_changes.push_back(0);
                }
                Instruction::AddX(x) => {
                    upcoming_x_changes.push_back(0);
                    upcoming_x_changes.push_back(*x);
                }
            }
        }

        //println!("Mid   cycles: {cycles} cycle: {cycle} x: {x} {:?}", upcoming_x_changes);

        // it is //during//
        if ((cycle + 20) % 40) == 0 {
            //println!("Cycle: {cycle} x:{x}  {}", (cycle as i64) * x);
            signal += (cycle as i64) * x;
        }

        // do soemthing with the instruction set
        if upcoming_x_changes.is_empty() {
        } else {
            x += upcoming_x_changes[0];
            upcoming_x_changes.pop_front();
        }

        //println!("End   cycles: {cycles} cycle: {cycle} x: {x} {:?}", upcoming_x_changes);
    }

    (x, signal, output.iter().collect())
}

pub fn part1_1(input: &[Instruction]) -> i64 {
    run_cycles(input, 220).1
}

pub fn part2_1(input: &[Instruction]) -> String {
    run_cycles(input, 240).2
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &'static str = "noop
addx 3
addx -5";

    const TESTS0: [(&str, usize, i64); 5] = [
        // formatting
        (EXAMPLE, 1, 1),
        (EXAMPLE, 2, 1),
        (EXAMPLE, 3, 4),
        (EXAMPLE, 4, 4),
        (EXAMPLE, 5, -1),
    ];

    #[test]
    fn cycles_test_1() {
        for test in TESTS0 {
            assert_eq!(run_cycles(&generator(test.0), test.1).0, test.2);
        }
    }

    const EXAMPLE2: &'static str = "addx 15
addx -11
addx 6
addx -3
addx 5
addx -1
addx -8
addx 13
addx 4
noop
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx -35
addx 1
addx 24
addx -19
addx 1
addx 16
addx -11
noop
noop
addx 21
addx -15
noop
noop
addx -3
addx 9
addx 1
addx -3
addx 8
addx 1
addx 5
noop
noop
noop
noop
noop
addx -36
noop
addx 1
addx 7
noop
noop
noop
addx 2
addx 6
noop
noop
noop
noop
noop
addx 1
noop
noop
addx 7
addx 1
noop
addx -13
addx 13
addx 7
noop
addx 1
addx -33
noop
noop
noop
addx 2
noop
noop
noop
addx 8
noop
addx -1
addx 2
addx 1
noop
addx 17
addx -9
addx 1
addx 1
addx -3
addx 11
noop
noop
addx 1
noop
addx 1
noop
noop
addx -13
addx -19
addx 1
addx 3
addx 26
addx -30
addx 12
addx -1
addx 3
addx 1
noop
noop
noop
addx -9
addx 18
addx 1
addx 2
noop
noop
addx 9
noop
noop
noop
addx -1
addx 2
addx -37
addx 1
addx 3
noop
addx 15
addx -21
addx 22
addx -6
addx 1
noop
addx 2
addx 1
noop
addx -10
noop
noop
addx 20
addx 1
addx 2
addx 2
addx -6
addx -11
noop
noop
noop";

    const TESTS1: [(&str, usize, i64); 6] = [
        // formatting
        (EXAMPLE2, 20, 420),
        (EXAMPLE2, 60, 1560),
        (EXAMPLE2, 100, 3360),
        (EXAMPLE2, 140, 6300),
        (EXAMPLE2, 180, 9180),
        (EXAMPLE2, 220, 13140),
    ];

    #[test]
    fn cycles_test_2() {
        for test in TESTS1 {
            assert_eq!(run_cycles(&generator(test.0), test.1).1, test.2);
        }
    }

    // modified the sample one to be easier to read
    const EXAMPLE2_PART2: &str = "\
██░░██░░██░░██░░██░░██░░██░░██░░██░░██░░
███░░░███░░░███░░░███░░░███░░░███░░░███░
████░░░░████░░░░████░░░░████░░░░████░░░░
█████░░░░░█████░░░░░█████░░░░░█████░░░░░
██████░░░░░░██████░░░░░░██████░░░░░░████
███████░░░░░░░███████░░░░░░░███████░░░░░";

    const TESTS: [(&str, i64, &str); 1] = [
        // formatting
        (EXAMPLE2, 13140, EXAMPLE2_PART2),
    ];

    #[test]
    fn part1_test() {
        for test in TESTS {
            assert_eq!(part1_1(&generator(test.0)), test.1);
        }
    }

    #[test]
    fn part2_test() {
        for test in TESTS {
            assert_eq!(part2_1(&generator(test.0)), test.2);
        }
    }
}
