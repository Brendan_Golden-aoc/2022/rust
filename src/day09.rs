#[derive(Debug, Clone)]
pub struct Generator {
    instructions: Vec<(Direction, isize)>,
    start: (usize, usize),
    size: (usize, usize),
}

#[derive(Debug, Clone)]
pub enum Direction {
    Up,
    Down,
    Left,
    Right,
}

pub fn generator(input: &str) -> Generator {
    let mut instructions = vec![];

    let mut row = (0, 0, 0);
    let mut col = (0, 0, 0);
    for line in input.split('\n') {
        let split = line.split(' ').collect::<Vec<_>>();
        if split.len() < 2 {
            continue;
        }

        let distance = split[1].parse().unwrap_or(0);
        let direction = match split[0].chars().collect::<Vec<_>>()[0] {
            'U' => {
                row.0 -= distance;

                if row.0 < row.1 {
                    row.1 = row.0;
                }
                Direction::Up
            }
            'D' => {
                row.0 += distance;

                if row.0 > row.2 {
                    row.2 = row.0;
                }
                Direction::Down
            }
            'L' => {
                col.0 -= distance;

                if col.0 < col.1 {
                    col.1 = col.0;
                }
                Direction::Left
            }
            'R' => {
                col.0 += distance;

                if col.0 > col.2 {
                    col.2 = col.0;
                }
                Direction::Right
            }
            _ => {
                continue;
            }
        };

        instructions.push((direction, distance));
    }

    if row.1 < 0 {
        let abs = -row.1;
        row.0 += abs;
        row.1 = abs;
        row.2 += abs;
    }
    if col.1 < 0 {
        let abs = -col.1;
        col.0 += abs;
        col.1 = abs;
        col.2 += abs;
    }

    Generator { instructions, start: (row.1 as usize, col.1 as usize), size: (row.2 as usize, col.2 as usize) }
}

pub fn part1_1(input: &Generator) -> i64 {
    calc(input, 2)
}

fn calc(input: &Generator, knots: usize) -> i64 {
    let mut board = vec![vec![0; input.size.1 + 1]; input.size.0 + 1];
    let mut bodies = vec![input.start; knots];
    for instruction in &input.instructions {
        for _ in 0..instruction.1 {
            for body in 0..bodies.len() {
                if body == 0 {
                    // head is here

                    // head moves
                    match instruction.0 {
                        Direction::Up => {
                            bodies[body].0 -= 1;
                        }
                        Direction::Down => {
                            bodies[body].0 += 1;
                        }
                        Direction::Left => {
                            bodies[body].1 -= 1;
                        }
                        Direction::Right => {
                            bodies[body].1 += 1;
                        }
                    }
                } else {
                    // tail is ehre

                    // everything follows the previous item
                    let last = bodies[body - 1];
                    let item = bodies[body];

                    // check same col
                    if item.0 == last.0 {
                        // same row
                        // moves back or forth
                        let distance = last.1 as isize - item.1 as isize;
                        if distance < -1 {
                            bodies[body].1 -= 1;
                        } else if distance > 1 {
                            bodies[body].1 += 1;
                        }
                    } else if item.1 == last.1 {
                        // same col
                        let distance = last.0 as isize - item.0 as isize;
                        if distance < -1 {
                            bodies[body].0 -= 1;
                        } else if distance > 1 {
                            bodies[body].0 += 1;
                        }
                    } else {
                        // check diagonals

                        let row_distance = last.0 as isize - item.0 as isize;
                        let col_distance = last.1 as isize - item.1 as isize;

                        let mut row_offset = row_distance;
                        let mut col_offset = col_distance;

                        if row_offset < 0 {
                            row_offset *= -1;
                        }
                        if col_offset < 0 {
                            col_offset *= -1;
                        }

                        if (row_offset * col_offset) > 1 {
                            // the last piece is not diagonal, thus this piece has to move

                            if row_offset == col_offset {
                                // move it in a diagonal pattern

                                if row_distance < -1 {
                                    bodies[body].0 -= 1;
                                } else {
                                    bodies[body].0 += 1;
                                }
                                if col_distance < -1 {
                                    bodies[body].1 -= 1;
                                } else {
                                    bodies[body].1 += 1;
                                }
                            } else {
                                if row_distance < -1 {
                                    bodies[body].0 -= 1;
                                    if col_distance < 0 {
                                        bodies[body].1 -= 1;
                                    } else {
                                        bodies[body].1 += 1;
                                    }
                                }
                                if row_distance > 1 {
                                    bodies[body].0 += 1;
                                    //*
                                    if col_distance < 0 {
                                        bodies[body].1 -= 1;
                                    } else {
                                        bodies[body].1 += 1;
                                    }
                                }

                                if col_distance < -1 {
                                    bodies[body].1 -= 1;
                                    //*
                                    if row_distance < 0 {
                                        bodies[body].0 -= 1;
                                    } else {
                                        bodies[body].0 += 1;
                                    }
                                }
                                if col_distance > 1 {
                                    bodies[body].1 += 1;
                                    //*
                                    if row_distance < 0 {
                                        bodies[body].0 -= 1;
                                    } else {
                                        bodies[body].0 += 1;
                                    }
                                }
                            }
                        }
                    }

                    // if the last is at a diagonal tehn ignore

                    // update the board to show where teh tail was
                    if body == (bodies.len() - 1) {
                        board[bodies[body].0][bodies[body].1] = 1;
                    }
                }
            }
        }
    }

    let mut count = 0;
    for row in &board {
        for col in row {
            if col == &1 {
                count += 1;
            }
        }
    }

    count
}

pub fn part2_1(input: &Generator) -> i64 {
    calc(input, 10)
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &'static str = "R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2";

    const EXAMPLE2: &'static str = "R 5
U 8
L 8
D 3
R 17
D 10
L 25
U 20";

    const TESTS: [(&str, i64, i64); 2] = [
        // formatting
        (EXAMPLE, 13, 1),
        (EXAMPLE2, 88, 36),
    ];

    #[test]
    fn part1_test() {
        for test in TESTS {
            assert_eq!(part1_1(&generator(test.0)), test.1);
        }
    }

    #[test]
    fn part2_test() {
        for test in TESTS {
            assert_eq!(part2_1(&generator(test.0)), test.2);
        }
    }
}
