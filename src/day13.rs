use std::cmp::Ordering;

#[derive(Debug, Clone)]
pub struct Packet(Vec<PacketContents>);

#[derive(Debug, Clone)]
pub enum PacketContents {
    Number(i64),
    SubPacket(Vec<PacketContents>),
}

impl Packet {
    fn process(input: &str) -> Self {
        Packet(Self::process_packets(input))
    }

    fn process_packets(input: &str) -> Vec<PacketContents> {
        let mut packet_contents: Vec<PacketContents> = vec![];

        let mut items = vec![];
        let mut tmp = String::new();
        let mut in_bracket = -1;
        for char in input.chars() {
            match char {
                '[' => {
                    if in_bracket >= 0 {
                        tmp.push(char);
                    }
                    in_bracket += 1;
                }
                ']' => {
                    in_bracket -= 1;
                    if in_bracket >= 0 {
                        tmp.push(char);
                    }
                    //tmp.push(char);
                }
                ',' => {
                    if in_bracket > 0 {
                        tmp.push(char);
                    } else {
                        items.push(tmp);
                        tmp = String::new();
                    }
                }
                _ => {
                    tmp.push(char);
                }
            }
        }
        if !tmp.is_empty() {
            items.push(tmp);
        }

        for item in items {
            //println!("item: {}", item);
            if item.starts_with('[') {
                packet_contents.push(PacketContents::SubPacket(Self::process_packets(&item)));
            } else if let Ok(number) = item.parse() {
                packet_contents.push(PacketContents::Number(number));
            }
        }

        packet_contents
    }

    fn compare(&self, right: &Packet) -> bool {
        if let Some(x) = Self::compare_contents(&self.0, &right.0) {
            x
        } else {
            false
        }
    }

    fn compare_contents(left: &Vec<PacketContents>, right: &Vec<PacketContents>) -> Option<bool> {
        for (i, left_item) in left.iter().enumerate() {
            if i >= right.len() {
                return Some(false);
            }
            let right_item = &right[i];

            match (left_item, right_item) {
                // both numbers
                (PacketContents::Number(l), PacketContents::Number(r)) => {
                    if l < r {
                        return Some(true);
                    }
                    if l > r {
                        return Some(false);
                    }
                    // onto the next round
                }
                (PacketContents::Number(l), PacketContents::SubPacket(r)) => {
                    if let Some(x) = Self::compare_contents(&vec![PacketContents::Number(*l)], r) {
                        return Some(x);
                    }
                }
                (PacketContents::SubPacket(l), PacketContents::Number(r)) => {
                    if let Some(x) = Self::compare_contents(l, &vec![PacketContents::Number(*r)]) {
                        return Some(x);
                    }
                }
                // both lists
                (PacketContents::SubPacket(l), PacketContents::SubPacket(r)) => {
                    if let Some(x) = Self::compare_contents(l, r) {
                        return Some(x);
                    }
                }
            }

            if i == (left.len() - 1) && i < (right.len() - 1) {
                return Some(true);
            }
        }

        match left.len().cmp(&right.len()) {
            Ordering::Less => Some(true),
            Ordering::Equal => None,
            Ordering::Greater => Some(false),
        }
    }
}

pub fn generator(input: &str) -> Vec<(Packet, Packet)> {
    let mut packet_pairs = vec![];

    for pair in input.split("\n\n") {
        let packets = pair.split('\n').collect::<Vec<_>>();
        if packets.len() < 2 {
            continue;
        }
        packet_pairs.push((Packet::process(packets[0]), Packet::process(packets[1])));
    }

    packet_pairs
}

// can use a lot of my stuff from day 20 last eyar
pub fn part1_1(input: &[(Packet, Packet)]) -> usize {
    let mut sum = 0;
    for (i, (left, right)) in input.iter().enumerate() {
        if left.compare(right) {
            sum += i + 1;
        }
    }
    sum
}

pub fn part2_1(input: &[(Packet, Packet)]) -> usize {
    let mut packets = vec![];
    // add in teh two divider ones
    packets.push(Packet::process("[[2]]"));
    packets.push(Packet::process("[[6]]"));

    for (left, right) in input {
        packets.push(left.clone());
        packets.push(right.clone());
    }

    let length = packets.len();
    // now need to sort them
    quicksort(&mut packets, 0, (length - 1) as isize);

    packets.reverse();

    // now to find the one in question
    let mut position = (0, 0);
    let two = Packet::process("[[2]]");
    let six = Packet::process("[[6]]");

    let mut too_done = false;
    for (i, right) in packets.iter().enumerate() {
        if !too_done && two.compare(right) {
            position.0 = i;
            too_done = true;
        } else if six.compare(right) {
            position.1 = i;
            break;
        }
    }

    position.0 * position.1
}

fn quicksort(array: &mut [Packet], low: isize, high: isize) {
    if low < high {
        // pi is partitioning index, arr[pi] is now at right place

        let p = partition(array, low, high);

        // Before pi
        quicksort(array, low, p - 1);
        // After pi
        quicksort(array, p + 1, high);
    }
}

fn partition(array: &mut [Packet], low: isize, high: isize) -> isize {
    // pivot (Element to be placed at right position)

    // Index of smaller element and indicates the
    let mut i = low - 1;
    // right position of pivot found so far

    let mut j = low;
    loop {
        if j > (high - 1) {
            break;
        }

        // If current element is smaller than the pivot
        if array[high as usize].compare(&array[j as usize]) {
            // increment index of smaller element
            i += 1;
            array.swap(i as usize, j as usize);
        }

        j += 1;
    }
    array.swap((i + 1) as usize, high as usize);

    (i + 1) as isize
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &'static str = "\
[1,1,3,1,1]
[1,1,5,1,1]

[[1],[2,3,4]]
[[1],4]

[9]
[[8,7,6]]

[[4,4],4,4]
[[4,4],4,4,4]

[7,7,7,7]
[7,7,7]

[]
[3]

[[[]]]
[[]]

[1,[2,[3,[4,[5,6,7]]]],8,9]
[1,[2,[3,[4,[5,6,0]]]],8,9]";

    const EXAMPLE2: &'static str = "\
[1,1,5,1,1]
[1,1,3,1,1]

[[1],4]
[[1],[2,3,4]]

[[8,7,6]]
[9]

[[4,4],4,4,4]
[[4,4],4,4]

[7,7,7]
[7,7,7,7]

[3]
[]

[[]]
[[[]]]

[1,[2,[3,[4,[5,6,0]]]],8,9]
[1,[2,[3,[4,[5,6,7]]]],8,9]";

    const TESTS: [(&str, usize, usize); 2] = [
        // formatting
        (EXAMPLE, 13, 140),
        (EXAMPLE2, 23, 140),
    ];

    #[test]
    fn part1_test() {
        for test in TESTS {
            assert_eq!(part1_1(&generator(test.0)), test.1);
        }
    }

    #[test]
    fn part2_test() {
        for test in TESTS {
            assert_eq!(part2_1(&generator(test.0)), test.2);
        }
    }
}
