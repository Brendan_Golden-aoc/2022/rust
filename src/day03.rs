use std::collections::{HashMap, HashSet};

pub fn generator(input: &str) -> Vec<Vec<char>> {
    let mut result = Vec::new();
    for line in input.split('\n') {
        result.push(line.chars().collect::<Vec<_>>());
    }
    result
}

pub fn part1_1(input: &[Vec<char>]) -> i32 {
    let mut max = 0;

    'outer: for bag in input {
        for i in (bag.len() / 2)..bag.len() {
            for j in 0..(bag.len() / 2) {
                if bag[i] == bag[j] {
                    max += get_priority(bag[i]);
                    continue 'outer;
                }
            }
        }
    }

    max
}

fn get_priority(a: char) -> i32 {
    match a {
        'a'..='z' => (a as i32 - 'a' as i32) + 1,
        'A'..='Z' => (a as i32 - 'A' as i32) + 27,
        _ => 0,
    }
}

pub fn part2_1(input: &[Vec<char>]) -> i32 {
    let mut counter = 0;

    let mut store: HashMap<char, i8> = HashMap::new();
    let mut elf = 1;
    for bag in input {
        let mut seen: HashSet<char> = HashSet::new();
        for c in bag {
            if seen.get(c).is_some() {
                continue;
            } else {
                seen.insert(*c);
            }

            if let Some(x) = store.get_mut(c) {
                *x += elf;
            } else {
                // wasnt in teh first elf's bag
                if elf > 1 {
                    continue;
                }
                store.insert(*c, elf);
            }
        }

        elf += 1;

        if elf > 3 {
            // get teh one that is 6
            for (key, val) in store.iter_mut() {
                if val == &6 {
                    counter += get_priority(*key);
                }
                *val = 0;
            }

            // reset for next round
            elf = 1;
            //store.clear();
        }
    }

    counter
}

pub fn part2_2(input: &[Vec<char>]) -> i32 {
    let mut counter = 0;

    let mut elf = 1;
    let mut shared = vec![];
    for bag in input {
        match elf {
            1 => {
                shared = bag.clone();
                elf += 1;
            }
            2 => {
                let mut tmp = vec![];
                for item in bag {
                    for item_shared in &shared {
                        if item == item_shared {
                            tmp.push(*item);
                        }
                    }
                }
                shared = tmp;
                elf += 1;
            }
            3 => {
                let mut tmp = vec![];
                for item in bag {
                    for item_shared in &shared {
                        if item == item_shared {
                            tmp.push(*item);
                        }
                    }
                }

                tmp.dedup();

                if tmp.len() == 1 {
                    counter += get_priority(tmp[0]);
                }

                // reset for next round
                elf = 1;
            }
            _ => {}
        }
    }

    counter
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &'static str = "vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw";

    #[test]
    fn part1_test() {
        assert_eq!(part1_1(&generator(EXAMPLE)), 157);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2_1(&generator(EXAMPLE)), 70);
    }

    #[test]
    fn part2_2_test() {
        assert_eq!(part2_2(&generator(EXAMPLE)), 70);
    }
}
