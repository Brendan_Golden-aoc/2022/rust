use std::cmp::Ordering;

pub fn generator(input: &str) -> Vec<Vec<i8>> {
    let mut result: Vec<Vec<i8>> = vec![];

    for line in input.split('\n') {
        result.push(line.chars().map(|c| c.to_digit(10).unwrap() as i8).collect());
    }

    result
}

pub fn part1_1(input: &Vec<Vec<i8>>) -> i64 {
    let rows = input.len();
    let cols = if input.is_empty() { 0 } else { input[0].len() };
    let mut visible = vec![vec![0; cols]; rows];

    let mut counter = 0;
    for row in 0..rows {
        let mut max = -1;

        // left to right
        for col in 0..cols {
            let tree = input[row][col];
            if tree > max {
                if visible[row][col] == 0 {
                    visible[row][col] = 1;
                    counter += 1;
                }

                max = tree;
            }
            if tree == 9 {
                break;
            }
        }

        max = -1;
        // right to left
        for col in (0..cols).rev() {
            let tree = input[row][col];
            if tree > max {
                if visible[row][col] == 0 {
                    visible[row][col] = 1;
                    counter += 1;
                }
                max = tree;
            }
            if tree == 9 {
                break;
            }
        }
    }

    for col in 0..cols {
        let mut max = -1;

        // top to bottom
        for row in 0..rows {
            let tree = input[row][col];
            if tree > max {
                if visible[row][col] == 0 {
                    visible[row][col] = 1;
                    counter += 1;
                }
                max = tree;
            }
            if tree == 9 {
                break;
            }
        }

        max = -1;
        // bottom to top
        for row in (0..rows).rev() {
            let tree = input[row][col];
            if tree > max {
                if visible[row][col] == 0 {
                    visible[row][col] = 1;
                    counter += 1;
                }
                max = tree;
            }
            if tree == 9 {
                break;
            }
        }
    }

    counter
}

pub fn part2_1(input: &Vec<Vec<i8>>) -> i64 {
    let rows = input.len();
    let cols = if input.is_empty() { 0 } else { input[0].len() };

    let mut max = 0;
    for row in 0..rows {
        for col in 0..cols {
            let result = check_cell(row, col, input, rows, cols);
            if result > max {
                max = result;
            }
        }
    }
    max
}

fn check_cell(row: usize, col: usize, input: &Vec<Vec<i8>>, rows: usize, cols: usize) -> i64 {
    let up = check_vertical(rows, -1, input[row][col], row, col, input);
    let down = check_vertical(rows, 1, input[row][col], row, col, input);

    let left = check_horizontal(cols, -1, input[row][col], row, col, input);
    let right = check_horizontal(cols, 1, input[row][col], row, col, input);

    up * down * left * right
}

fn check_vertical(rows: usize, increment: isize, start: i8, row: usize, col: usize, input: &Vec<Vec<i8>>) -> i64 {
    match increment.cmp(&0) {
        Ordering::Less => {
            if row == 0 {
                return 0;
            }
        }
        Ordering::Equal => {
            return 0;
        }
        Ordering::Greater => {
            if row == (rows - 1) {
                return 0;
            }
        }
    }

    // look up
    let row_new = (row as isize + increment) as usize;
    if input[row_new][col] >= start {
        // met a blocking tree, this only counts as 1
        1
    } else {
        1 + check_vertical(rows, increment, start, row_new, col, input)
    }
}

fn check_horizontal(cols: usize, increment: isize, start: i8, row: usize, col: usize, input: &Vec<Vec<i8>>) -> i64 {
    match increment.cmp(&0) {
        Ordering::Less => {
            if col == 0 {
                return 0;
            }
        }
        Ordering::Equal => {
            return 0;
        }
        Ordering::Greater => {
            if col == (cols - 1) {
                return 0;
            }
        }
    }

    // look up
    let col_new = (col as isize + increment) as usize;
    if input[row][col_new] >= start {
        // met a blocking tree, this only counts as 1
        1
    } else {
        1 + check_horizontal(cols, increment, start, row, col_new, input)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &'static str = "30373
25512
65332
33549
35390";

    const TESTS: [(&str, i64, i64); 1] = [(EXAMPLE, 21, 8)];

    #[test]
    fn part1_test() {
        for test in TESTS {
            assert_eq!(part1_1(&generator(test.0)), test.1);
        }
    }

    #[test]
    fn part2_test() {
        for test in TESTS {
            assert_eq!(part2_1(&generator(test.0)), test.2);
        }
    }
}
