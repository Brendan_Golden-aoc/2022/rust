use std::collections::HashMap;

pub fn generator(input: &str) -> HashMap<String, i64> {
    let mut result = HashMap::new();
    let mut path = vec![];

    for line in input.split('\n') {
        let split = line.split(' ').collect::<Vec<_>>();
        if line.starts_with('$') {
            if split[1].starts_with("cd") {
                if split.len() < 3 {
                    continue;
                }

                let next_folder = split[2].trim();
                if next_folder == ".." {
                    // go iup a level
                    // remove last item from teh array (arraylist)
                    path.pop();
                } else {
                    // going deeper, add it onto teh end of the array
                    path.push(next_folder.to_owned());
                }
            }
        } else if !line.starts_with("dir") {
            let size = split[0].parse().unwrap_or(100);

            let mut path_to_here = String::new();
            for parent in &path {
                // build up teh full path
                path_to_here = format!("{}/{}", path_to_here, parent);

                match result.get(&path_to_here) {
                    // does nto exist in teh hashmap, add it
                    None => {
                        result.insert(path_to_here.clone(), size);
                    }
                    // exists, grab its value and add to it
                    Some(prev) => {
                        result.insert(path_to_here.clone(), size + *prev);
                    }
                }
            }
        }
    }

    result
}

pub fn part1_1(input: &HashMap<String, i64>) -> i64 {
    let mut counter = 0;
    for x in input.values() {
        if *x <= 100000 {
            counter += *x;
        }
    }

    counter
}

pub fn part2_1(input: &HashMap<String, i64>) -> i64 {
    let filesystem = 70000000;
    // because of implementation the root is // in teh hashmap
    let used = input.get("//").unwrap_or(&0);
    let free = filesystem - used;
    let required = 30000000 - free;

    let mut smallest = i64::MAX;
    for x in input.values() {
        if *x >= required && *x < smallest {
            smallest = *x;
        }
    }

    smallest
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &'static str = "$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k";

    // modified to have folders with same name in multiple palces
    const EXAMPLE2: &'static str = "$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ cd c
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd ..
$ cd d
$ cd c
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k";

    const TESTS: [(&str, i64, i64); 2] = [
        // multiple items to test edge cases
        (EXAMPLE, 95437, 24933642),
        (EXAMPLE2, 190290, 24933642),
    ];

    #[test]
    fn part1_test() {
        for test in TESTS {
            assert_eq!(part1_1(&generator(test.0)), test.1);
        }
    }

    #[test]
    fn part2_test() {
        for test in TESTS {
            assert_eq!(part2_1(&generator(test.0)), test.2);
        }
    }
}
