#[derive(Debug, Clone)]
pub struct Generator {
    terrain: Vec<Vec<i8>>,
    start: (usize, usize),
    end: (usize, usize),
    size: (usize, usize),
}

pub fn generator(input: &str) -> Generator {
    let mut terrain = vec![];
    let mut start = (0, 0);
    let mut end = (0, 0);
    let mut size = (0, 0);

    for (row, line) in input.split('\n').enumerate() {
        let mut tmp = vec![];
        for (col, char) in line.chars().enumerate() {
            match char {
                'S' => {
                    start = (row, col);
                    tmp.push(0);
                }
                'E' => {
                    end = (row, col);
                    tmp.push(26);
                }
                x => tmp.push(x as i8 - 'a' as i8),
            }
            if col > size.1 {
                size.1 = col;
            }
        }
        terrain.push(tmp);

        if row > size.0 {
            size.0 = row;
        }
    }

    Generator { terrain, start, end, size }
}

// can use a lot of my stuff from day 20 last eyar
pub fn part1_1(input: &Generator) -> i64 {
    solve(input, input.start)
}

fn solve(input: &Generator, start: (usize, usize)) -> i64 {
    let mut map = vec![vec![i64::MAX; input.size.1 + 1]; input.size.0 + 1];

    // set starting position to 0
    map[start.0][start.1] = 0;

    let mut to_visit = vec![start];

    while !to_visit.is_empty() {
        // go to first entry
        let current = to_visit[0];

        let (row, col) = current;

        let current_distance = map[row][col];

        if current == input.end {
            return current_distance;
        }

        // check if it already visited

        // update neighbours

        //println!("{:?} {}", current, current_distance);
        // up?
        if row > 0 {
            //println!("up");
            if let Some(x) = test_cell(&input.terrain, &map, current, (row - 1, col)) {
                map[x.0][x.1] = current_distance + 1;
                to_visit.push(x);
            }
        }

        // down
        if row < input.size.0 {
            //println!("down");
            if let Some(x) = test_cell(&input.terrain, &map, current, (row + 1, col)) {
                map[x.0][x.1] = current_distance + 1;
                to_visit.push(x);
            }
        }

        // right
        if col < input.size.1 {
            //println!("right");
            if let Some(x) = test_cell(&input.terrain, &map, current, (row, col + 1)) {
                map[x.0][x.1] = current_distance + 1;
                to_visit.push(x);
            }
        }

        // left?
        if col > 0 {
            //println!("left");
            if let Some(x) = test_cell(&input.terrain, &map, current, (row, col - 1)) {
                map[x.0][x.1] = current_distance + 1;
                to_visit.push(x);
            }
        }

        // remove first entry
        to_visit.remove(0);
    }

    i64::MAX
}

fn test_cell(terrain: &[Vec<i8>], map: &[Vec<i64>], current: (usize, usize), test: (usize, usize)) -> Option<(usize, usize)> {
    //println!("Test: {:?}", test);
    let height_current = terrain[current.0][current.1];
    let height = terrain[test.0][test.1];
    if (height - 1) <= height_current && (map[current.0][current.1] + 1) < map[test.0][test.1] {
        //println!("Next: {:?}",(test.0, test.1) );
        return Some((test.0, test.1));
    }

    None
}

pub fn part2_1(input: &Generator) -> i64 {
    let mut min = i64::MAX;
    for (r, row) in input.terrain.iter().enumerate() {
        for (c, col) in row.iter().enumerate() {
            if col == &0 {
                let result = solve(input, (r, c));
                if result < min {
                    min = result;
                }
            }
        }
    }

    min
}

pub fn part2_2(input: &Generator) -> i64 {
    let mut map = vec![vec![i64::MAX; input.size.1 + 1]; input.size.0 + 1];

    // set starting position to 0
    map[input.end.0][input.end.1] = 0;

    let mut to_visit = vec![input.end];

    while !to_visit.is_empty() {
        // go to first entry
        let current = to_visit[0];

        let (row, col) = current;

        let current_distance = map[row][col];

        // check if it already visited

        // update neighbours

        //println!("{:?} {}", current, current_distance);
        // up?
        if row > 0 {
            //println!("up");
            if let Some(x) = test_cell_2(&input.terrain, &map, current, (row - 1, col)) {
                map[x.0][x.1] = current_distance + 1;
                to_visit.push(x);
            }
        }

        // down
        if row < input.size.0 {
            //println!("down");
            if let Some(x) = test_cell_2(&input.terrain, &map, current, (row + 1, col)) {
                map[x.0][x.1] = current_distance + 1;
                to_visit.push(x);
            }
        }

        // right
        if col < input.size.1 {
            //println!("right");
            if let Some(x) = test_cell_2(&input.terrain, &map, current, (row, col + 1)) {
                map[x.0][x.1] = current_distance + 1;
                to_visit.push(x);
            }
        }

        // left?
        if col > 0 {
            //println!("left");
            if let Some(x) = test_cell_2(&input.terrain, &map, current, (row, col - 1)) {
                map[x.0][x.1] = current_distance + 1;
                to_visit.push(x);
            }
        }

        // remove first entry
        to_visit.remove(0);
    }

    let mut min = i64::MAX;

    for (r, row) in input.terrain.iter().enumerate() {
        for (c, col) in row.iter().enumerate() {
            // only on a's
            if col == &0 {
                let result = map[r][c];
                if result < min {
                    min = result;
                }
            }
        }
    }

    min
}

fn test_cell_2(terrain: &[Vec<i8>], map: &[Vec<i64>], current: (usize, usize), test: (usize, usize)) -> Option<(usize, usize)> {
    //println!("Test: {:?}", test);
    let height_current = terrain[current.0][current.1];
    let height = terrain[test.0][test.1];
    if ((height + 1) == height_current || height >= height_current) && (map[current.0][current.1] + 1) < map[test.0][test.1] {
        return Some((test.0, test.1));
    }

    None
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &'static str = "\
Sabqponm
abcryxxl
accszExk
acctuvwj
abdefghi";

    const TESTS: [(&str, i64, i64); 1] = [
        // formatting
        (EXAMPLE, 31, 29),
    ];

    #[test]
    fn part1_test() {
        for test in TESTS {
            assert_eq!(part1_1(&generator(test.0)), test.1);
        }
    }

    #[test]
    fn part2_test() {
        for test in TESTS {
            assert_eq!(part2_1(&generator(test.0)), test.2);
        }
    }

    #[test]
    fn part2_2_test() {
        for test in TESTS {
            assert_eq!(part2_2(&generator(test.0)), test.2);
        }
    }
}
