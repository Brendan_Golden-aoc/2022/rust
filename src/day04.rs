pub fn generator(input: &str) -> Vec<[TimeRange; 2]> {
    let mut result = Vec::new();
    for line in input.split('\n') {
        let line_s = line.split(',').collect::<Vec<_>>();
        result.push([generator_sub(line_s[0]), generator_sub(line_s[1])]);
    }
    result
}

#[derive(Debug)]
pub struct TimeRange {
    start: i32,
    end: i32,
}

fn generator_sub(input: &str) -> TimeRange {
    let split = input.split('-').collect::<Vec<_>>();
    let start = if let Ok(x) = split[0].parse::<i32>() { x } else { 0 };
    let end = if let Ok(x) = split[1].parse::<i32>() { x } else { 0 };
    TimeRange { start, end }
}

pub fn part1_1(input: &[[TimeRange; 2]]) -> i32 {
    let mut counter = 0;

    for line in input {
        if (line[0].start >= line[1].start && line[0].start <= line[1].end && line[0].end >= line[1].start && line[0].end <= line[1].end) ||
            // formatting
           (line[1].start >= line[0].start && line[1].start <= line[0].end && line[1].end >= line[0].start && line[1].end <= line[0].end)
        {
            counter += 1;
        }
    }

    counter
}

pub fn part2_1(input: &[[TimeRange; 2]]) -> i32 {
    let mut counter = 0;

    for line in input {
        if (line[0].start >= line[1].start && line[0].start <= line[1].end) ||
            // formatting
            (line[0].end >= line[1].start && line[0].end <= line[1].end) ||
            // formatting
            (line[1].start >= line[0].start && line[1].start <= line[0].end) ||
            // formatting
            (line[1].end >= line[0].start && line[1].end <= line[0].end)
        {
            counter += 1;
        }
    }

    counter
}

// https://stackoverflow.com/questions/325933/determine-whether-two-date-ranges-overlap
pub fn part2_2(input: &[[TimeRange; 2]]) -> i32 {
    let mut counter = 0;

    for line in input {
        if line[0].start <= line[1].end && line[1].start <= line[0].end {
            counter += 1;
        }
    }

    counter
}

pub fn part1_2(input: &[[TimeRange; 2]]) -> i32 {
    let mut counter = 0;

    for line in input {
        // check for overlap first
        if line[0].start <= line[1].end
            && line[1].start <= line[0].end
            && (
                // then check if one is contained within teh other
                (line[0].start >= line[1].start && line[0].end >= line[1].start && line[0].end <= line[1].end) || // alignment so one is directly under teh otehr
                (line[1].start >= line[0].start && line[1].end >= line[0].start && line[1].end <= line[0].end)
            )
        {
            counter += 1;
        }
    }

    counter
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &'static str = "2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8";

    #[test]
    fn part1_test() {
        assert_eq!(part1_1(&generator(EXAMPLE)), 2);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2_1(&generator(EXAMPLE)), 4);
    }

    #[test]
    fn part2_2_test() {
        assert_eq!(part2_2(&generator(EXAMPLE)), 4);
    }

    #[test]
    fn part1_2_test() {
        assert_eq!(part1_2(&generator(EXAMPLE)), 2);
    }
}
