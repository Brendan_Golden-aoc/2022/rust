pub fn generator(input: &str) -> Vec<char> {
    input.chars().collect()
}

pub fn part1_0(input: &[char]) -> usize {
    let mut counter = 3;

    loop {
        if counter >= input.len() {
            break;
        }

        if input[counter] != input[counter-1] &&
            // formatting
            input[counter] != input[counter-2] &&
            input[counter] != input[counter-3] &&

            input[counter - 1] != input[counter-2] &&
            input[counter - 1] != input[counter-3] &&

            input[counter - 2] != input[counter-3]
        {
            break;
        }
        counter += 1;
    }

    counter + 1
}

pub fn part1_1(input: &[char]) -> usize {
    let size = 4;
    let mut counter = size - 1;

    loop {
        if counter >= input.len() {
            break;
        }
        if check_last_n_chars(input, counter, size - 1) {
            break;
        }

        counter += 1;
    }

    counter + 1
}

fn check_last_n_chars(input: &[char], counter: usize, n: usize) -> bool {
    for left in (counter - n)..counter {
        for right in ((counter - n) + 1)..=counter {
            if left == right {
                continue;
            }

            //println!("{}  left: {} {} Right:{}  {}",counter, left, input[left], right, input[right]);
            if input[left] == input[right] {
                return false;
            }
        }
    }
    true
}

pub fn part2_1(input: &[char]) -> usize {
    let size = 14;
    let mut counter = size - 1;

    loop {
        if counter >= input.len() {
            break;
        }
        if check_last_n_chars(input, counter, size - 1) {
            break;
        }

        counter += 1;
    }

    counter + 1
}

pub fn part1_2(input: &[char]) -> usize {
    let size = 4;
    let mut counter = size - 1;

    loop {
        if counter >= input.len() {
            break;
        }
        if check_last_n_chars_2(input, counter, size - 1) {
            break;
        }

        counter += 1;
    }

    counter + 1
}

pub fn part2_2(input: &[char]) -> usize {
    let size = 14;
    let mut counter = size - 1;

    loop {
        if counter >= input.len() {
            break;
        }
        if check_last_n_chars_2(input, counter, size - 1) {
            break;
        }

        counter += 1;
    }

    counter + 1
}

fn check_last_n_chars_2(input: &[char], counter: usize, n: usize) -> bool {
    for left in (counter - n)..counter {
        for right in left..=counter {
            if left == right {
                continue;
            }

            //println!("{}  left: {} {} Right:{}  {}",counter, left, input[left], right, input[right]);
            if input[left] == input[right] {
                return false;
            }
        }
    }
    true
}

// exploring https://www.mattkeeter.com/blog/2022-12-10-xor/
pub fn part1_3(input: &[char]) -> usize {
    check_last_n_chars_3(input, 4)
}

pub fn part2_3(input: &[char]) -> usize {
    check_last_n_chars_3(input, 14)
}

fn check_last_n_chars_3(input: &[char], window_size: usize) -> usize {
    let mut set: i32 = 0;
    for i in 0..input.len() {
        // Turn on bits as they enter the window
        set ^= 1 << (input[i] as u32 - 'a' as u32);

        // Turn off bits as they leave the window
        if i >= window_size {
            set ^= 1 << (input[i - window_size] as u32 - 'a' as u32);
        }

        // Check the current window and see if we're done
        if set.count_ones() as usize == window_size {
            return i + 1;
        }
    }
    0
}

#[cfg(test)]
mod tests {
    use super::*;

    // (input, part1, part2)
    const TESTS: [(&str, usize, usize); 5] = [
        // formatting
        ("mjqjpqmgbljsphdztnvjfqwrcgsmlb", 7, 19),
        ("bvwbjplbgvbhsrlpgdmjqwftvncz", 5, 23),
        ("nppdvjthqldpwncqszvftbrmjlhg", 6, 23),
        ("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", 10, 29),
        ("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", 11, 26),
    ];

    #[test]
    fn part1_0_test() {
        for test in TESTS {
            assert_eq!(part1_0(&generator(test.0)), test.1);
        }
    }

    #[test]
    fn part1_1_test() {
        for test in TESTS {
            assert_eq!(part1_1(&generator(test.0)), test.1);
        }
    }

    #[test]
    fn part1_2_test() {
        for test in TESTS {
            assert_eq!(part1_2(&generator(test.0)), test.1);
        }
    }

    #[test]
    fn part1_3_test() {
        for test in TESTS {
            assert_eq!(part1_3(&generator(test.0)), test.1);
        }
    }

    #[test]
    fn part2_1_test() {
        for test in TESTS {
            assert_eq!(part2_1(&generator(test.0)), test.2);
        }
    }

    #[test]
    fn part2_2_test() {
        for test in TESTS {
            assert_eq!(part2_2(&generator(test.0)), test.2);
        }
    }

    #[test]
    fn part2_3_test() {
        for test in TESTS {
            assert_eq!(part2_3(&generator(test.0)), test.2);
        }
    }
}
