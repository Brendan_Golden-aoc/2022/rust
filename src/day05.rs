#[derive(Debug, Clone)]
pub struct Generator {
    piles: Vec<Vec<char>>,
    instructions: Vec<GeneratorInstructions>,
}

impl Generator {
    fn init() -> Self {
        Generator { piles: vec![], instructions: vec![] }
    }

    fn piles(&mut self, input: &str) {
        let lines = input.split('\n').collect::<Vec<_>>();

        // j is the col
        let mut col = 1;
        loop {
            if col > lines[lines.len() - 1].len() {
                break;
            }

            let mut pile = vec![];

            // start on teh second last row
            let mut row = lines.len() - 2;
            // check if teh current row is long enough

            loop {
                let row_line = lines[row].chars().collect::<Vec<_>>();
                if col < row_line.len() && row_line[col] != ' ' {
                    pile.push(row_line[col]);
                }

                // go one row up
                if row == 0 {
                    break;
                } else {
                    row -= 1;
                }
            }

            self.piles.push(pile);

            col += 4;
        }
    }

    fn instructions(&mut self, input: &str) {
        for instruction in input.split('\n') {
            if let Some(x) = GeneratorInstructions::format(instruction) {
                self.instructions.push(x);
            }
        }
    }
}
#[derive(Debug, Clone)]
pub struct GeneratorInstructions {
    quantity: usize,
    from: usize,
    to: usize,
}

impl GeneratorInstructions {
    fn format(line: &str) -> Option<Self> {
        // move 3 from 8 to 9
        let split = line.split(' ').collect::<Vec<_>>();

        if split.len() < 6 {
            return None;
        }

        let quantity = split[1].parse::<usize>().unwrap_or(0);
        // going to use the index values
        let from = split[3].parse::<usize>().unwrap_or(1) - 1;
        let to = split[5].parse::<usize>().unwrap_or(1) - 1;

        Some(GeneratorInstructions { quantity, from, to })
    }
}

pub fn generator(input: &str) -> Generator {
    let sections = input.split("\n\n").collect::<Vec<_>>();

    let mut result = Generator::init();
    result.piles(sections[0]);
    result.instructions(sections[1]);

    result
}

pub fn part1_1(input: &Generator) -> String {
    let mut piles = input.piles.clone();
    for instruction in &input.instructions {
        let length = piles[instruction.from].len();
        let mut col_split = piles[instruction.from].split_off(length - instruction.quantity);
        col_split.reverse();
        piles[instruction.to].append(&mut col_split);
    }

    let mut result = String::new();
    for pile in piles {
        result.push(pile[pile.len() - 1]);
    }

    result
}

pub fn part2_1(input: &Generator) -> String {
    let mut piles = input.piles.clone();
    for instruction in &input.instructions {
        let length = piles[instruction.from].len();
        let mut col_split = piles[instruction.from].split_off(length - instruction.quantity);
        //col_split.reverse();
        piles[instruction.to].append(&mut col_split);
    }

    let mut result = String::new();
    for pile in piles {
        result.push(pile[pile.len() - 1]);
    }

    result
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &'static str = "    [D]
[N] [C]
[Z] [M] [P]
 1   2   3

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2";

    #[test]
    fn part1_test() {
        assert_eq!(part1_1(&generator(EXAMPLE)), "CMZ");
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2_1(&generator(EXAMPLE)), "MCD");
    }
}
