mod day01;
mod day02;
mod day03;
mod day04;
mod day05;
mod day06;
mod day07;
mod day08;
mod day09;
mod day10;
mod day11;
mod day12;
mod day13;

aoc_main::main! {
    year 2022;
    day01 : generator => part1_1, part2_1;
    day02 : generator => part1_1, part1_2, part1_3, part2_1, part2_2, part2_3;
    day02 : generator_4 => part1_4;
    day03 : generator => part1_1, part2_1, part2_2;
    day04 : generator => part1_1, part1_2, part2_1, part2_2;
    day05 : generator => part1_1, part2_1;
    day06 : generator => part1_0, part1_1, part1_2, part1_3, part2_1, part2_2, part2_3;
    day07 : generator => part1_1, part2_1;
    day08 : generator => part1_1, part2_1;
    day09 : generator => part1_1, part2_1;
    day10 : generator => part1_1, part2_1;
    day11 : generator => part1_1, part2_1;
    day12 : generator => part1_1, part2_1, part2_2;
    day13 : generator => part1_1, part2_1;
}
